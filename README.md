Requirements to this app:

1. DarkSky API key
2. Google geolocation key
3. Python 3 and pip3
4. Redis

To run:

1. create a virtual environment
2. install all the dependencies from requirements.txt
3. store your api keys in your environment or add them in app.py
4. run redis-server and app.py
5. open localhost