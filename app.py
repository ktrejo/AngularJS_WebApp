import os
import requests
import pprint
import redis
import json
import googlemaps
from flask import Flask, jsonify, request, render_template
from dotenv import load_dotenv

dotenv_path = os.path.join(os.path.dirname(__file__), '.env')
load_dotenv(dotenv_path)

# API keys and urls
DARK_SKY_API_KEY = os.environ['DARK_SKY_API_KEY']
GOOGLE_API_KEY = os.environ['GOOGLE_API_KEY']
DARK_SKY_URL = "https://api.darksky.net/forecast/"

# Redis database
redis_url = os.getenv('REDISTOGO_URL', 'redis://localhost:6379/0')
redis_conn = redis.StrictRedis(host='localhost', port=6379)


app = Flask(__name__)

@app.route('/')
def home():
    return render_template('index.html')


@app.route('/weather/api/v1/city/<city_name>', methods=['GET'])
def weather_by_city_name(city_name):

    weather_data = redis_conn.get(city_name)

    if weather_data is not None:
        return jsonify(json.loads(weather_data.decode('utf-8')))


    # get latitude and longitude from google maps
    gmaps = googlemaps.Client(key=GOOGLE_API_KEY)
    geocode_result = googlemaps.geocoding.geocode(gmaps, address=city_name)
    # geocode_result = gmaps.geocode('city_name')

    if geocode_result == []:
        return jsonify({'error' : 'Weather could not be obtain at this time.'})


    location = geocode_result[0]['geometry']['location']
    lat = str(location['lat'])
    lon = str(location['lng'])

    
    # get requests to dark sky
    url = DARK_SKY_URL + DARK_SKY_API_KEY
    url += '/' + lat + ',' + lon
    res = acquire_response(url=url)

    if res.status_code != 200:
        return jsonify({'error' : 'Weather could not be obtain at this time.'})

    weather_data = json.dumps(res.json())

    # store to redis with 1 hour expiration
    redis_conn.set(city_name, weather_data, ex=3600)


    return jsonify(res.json())



def acquire_response(url, params=None):
    """
    Attemps get request 3 times until successful status
    """
    x = 3
    status_200 = False
    res = None

    while x > 0 and status_200 == False:
        res = requests.get(url=url, params=params)
        if res.status_code == 200:
            status_200 = True

        x -= 1

    return res



if __name__ == '__main__':
    app.run(debug=True)


