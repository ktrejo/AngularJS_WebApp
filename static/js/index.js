(function(){
    

    var weatherApp = angular.module("weatherApp", [
        "ngRoute",
        "ngResource"
    ]);

    // Custom filters
    weatherApp.filter("tempDegreeUnit", function() {
        return function(kelvin, toggle) {
            // Kelvin to Fahrenheit
            if (toggle) 
                return (kelvin * 1.8) - 459.67;

            // Kelvin to Celsius
            else 
                return kelvin - 273.15;
            
        }
    });

    weatherApp.filter("fahToCel", function() {
        return function(fahrenheit, toggle) {
            // Fahrenheit to Celsius
            if (toggle)
                return (fahrenheit - 32)*(5/9);

            else
                return fahrenheit;
        }
    });

    weatherApp.filter("mphTokph", function() {
        return function(speed, toggle) {
            // Convert mph to kph
            if (toggle)
                return speed * 1.60934;

            else
                return speed;
        }

    });

    // Controllers
    weatherApp.controller("weatherController", ["$scope", "$http", "$log", function($scope, $http, $log) {

        $scope.success = false;
        $scope.displayError = false;
        $scope.location = null;
        $scope.changeUnit = false;
        $scope.unit = {
                        'weather': 'F',
                        'windSpeed' : 'mph',
                        'humid' : '%',
                        'rain' : '%'
                        };

        $scope.current = null;
        $scope.hourly = null;
        $scope.daily = null;
        $scope.view = 1;

        $scope.getForecast = function(inputLocation) {
            $http({
                method: 'GET',
                url: 'http://localhost:5000/weather/api/v1/city/' + inputLocation
            }).then(function(success) {
                //$log.log(success);

                $scope.location = inputLocation;

                $scope.current = success.data['currently'];
                $scope.hourly = success.data['hourly'];
                $scope.daily = success.data['daily'];

                $scope.success = true;
                $scope.displayError = false;

            }, function(error) {
                $log.log("fail to retrieve weather");
                $scope.success = false;
                $scope.displayError = true;
            })
        }

        $scope.toggleUnit = function() {
            $scope.changeUnit = !($scope.changeUnit);
            $scope.unit['weather'] = ($scope.unit['weather'] == 'F')? 'C' : 'F';
            $scope.unit['windSpeed'] = ($scope.unit['windSpeed'] == 'mph')? 'kph' : 'mph';
        }

        $scope.show = function(level) {
            $scope.view = level;
        }


    }]);

}());